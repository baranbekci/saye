// Common configuration files with defaults plus overrides from environment vars

let CommonUtils = require("./E2E/Shared/CommonUtils");
CommonUtils.getCliParams(); // set cli params

module.exports = {
    /**
     * The address of a running selenium server.
     *
     * @Docker Configuration
     * docker run -tid --name zalenium -p 4444:4444 -e ZALENIUM_EXTRA_JVM_PARAMS="-Dwebdriver.http.factory=apache" -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/videos:/home/seluser/videos -v /home/sysadmin/zalenium/:/tmp/node/home/seluser/folder/ -v /tmp/Downloads:/tmp/node/home/seluser/Downloads --privileged dosel/zalenium start --desiredContainers 1 --maxTestSessions 500 --maxDockerSeleniumContainers 8 --screenWidth 1920 --screenHeight 1080 --videoRecordingEnabled true --timeZone "Europe/Istanbul"  --debugEnabled false
     * docker run -d -p 8090:8080 -v /home/sysadmin/http-server-files:/public danjellz/http-server
     * http://10.100.30.194:4444/wd/hub
     *
     * @returns {string}
     */
    seleniumAddress: function () {
       return "";
    },

    capabilities: function() {
        let obj = { };

        obj['browserName'] = 'chrome';
        obj['idleTimeout'] = 500; // for zalenium idle timeout']

        obj['chromeOptions']= {
            'args': this.browserArgs(),
            'prefs': {
                profile: {
                    default_content_settings: {
                        popups: 0
                    },
                },
                download: {
                    'prompt_for_download': false,
                    'directory_upgrade': true,
                    'default_directory': this.downloadedBrowserDir()
                }
            }
        };
        return obj;
    },

    browserArgs: function () {
        // https://github.com/GoogleChrome/chrome-launcher/blob/master/docs/chrome-flags-for-tools.md
        let args = ['disable-infobars', '--disable-web-security'];

        if(process.params.headless === "true")
            args = args.concat(['headless', 'remote-debugging-port=9222']);

        return args;
    },

    downloadedBrowserDir: function () {

        return __dirname + '\\misc\\download\\';
    },

    downloadedDir: function () {
        return __dirname + '\\misc\\download\\';
    },

    copyDir: function () {
        return __dirname + '\\misc\\download\\';
    },

    cucumberReportFormat: function () {
        return 'json:Reports/' + (process.params.jsonName || "cucumber_report") + '.json';
    },

    paths: {
        countLinesOfFeatures: __dirname + "/misc/tmp/countLinesOfFeatures.json",
        featureList: __dirname + "/misc/download/features.lines.json"
    }
};
