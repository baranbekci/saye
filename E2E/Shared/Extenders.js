"use strict";

/**
 * Click element because of problem of vertical scrolling
 * https://stackoverflow.com/questions/31922080/protractor-cant-add-new-method-to-return-findelement
 *
 * @example
 * element(by.id('selectPerson').clickOnHidden();
 *
 * @returns {Promise<protractor.ElementFinder>}
 */
protractor.ElementFinder.prototype.clickOnHidden = async function () {
    const self = this;
    await self.showIntoView();

    await browser.wait(await until.elementToBeClickable(self), 3000);

    await self.click();

    return Promise.resolve();
};

/**
 * Send keys via execute script
 *
 * @example
 * element(by.id('selectPerson').sendKeysExecute('test');
 *
 * @param value
 * @returns {Promise.<protractor.ElementFinder>}
 */
protractor.ElementFinder.prototype.sendKeysExecute = async function (value) {
    const self = this;
    await self.showIntoView();

    await browser.wait(await until.elementToBeClickable(self), 3000);

    await browser.executeScript("arguments[0].setAttribute('value', '" + value +"')", self);

    return Promise.resolve();
};


/**
 * Click element via Js.
 * https://stackoverflow.com/questions/31922080/protractor-cant-add-new-method-to-return-findelement
 *
 * @example
 * element(by.id('selectPerson').clickPerform();
 *
 * @returns {Promise<protractor.ElementFinder>}
 */
protractor.ElementFinder.prototype.clickPerform = async function () {
    const self = this;
    await browser.wait(await until.elementToBeClickable(self), 5000);

    await browser.executeScript("arguments[0].click();", self);

    return Promise.resolve();
};

/**
 * Click on element specific location
 *
 * @example
 * element(by.id('selectPerson').clickSpecPoint({ x: 500, y: 600 });
 * element(by.id('selectPerson').clickSpecPoint(); // click center of element
 *
 * @param param
 * @param isDoubleClick
 * @returns {Promise.<protractor.ElementFinder>}
 */
protractor.ElementFinder.prototype.clickSpecPoint = async function (param, isDoubleClick) {
    const self = this;
    let coordinate = {
        x:0,
        y:0
    };

    if (param !== undefined) {
        coordinate.x = param.x || coordinate.x;
        coordinate.y = param.y || coordinate.y;
    }
    else
        coordinate = await self.getCenterOfElement();

    await WebUtils.clickSpecPointOnElement(self, coordinate.x, coordinate.y, isDoubleClick);
    await browser.sleep(1000); // wait for drawing poligons otherwise throw exception

    return Promise.resolve();
};

/**
 * Get center of element x and y coordinate
 *
 * @example
 * element(by.id('selectPerson').getCenterOfElement();
 *
 * @returns {Promise<{x: number, y: number}>}
 */
protractor.ElementFinder.prototype.getCenterOfElement = async function () {
    const self = this;
    let x = 0;
    let y = 0;

    await self.getSize().then(function (eleSize) {
        x = eleSize.width / 2;
        y = eleSize.height / 2;
    });

    return {
        x: x,
        y: y
    }
};

/**
 * Select Element By Value
 *
 * @example
 * element(by.id('selectPerson').selectByValue('1');
 *
 * @param value
 * @returns {Promise<void>}
 */
protractor.ElementFinder.prototype.selectByValue = async function (value) {
    const self = this;

    await self.$$('option[value="' + value + '"]').click();
};

/**
 * Select Element By Text
 *
 * @example
 * element(by.id('selectPerson').selectByText('Select 1');
 *
 * @param searchingText
 * @returns {Promise<void>}
 */
protractor.ElementFinder.prototype.selectByText = async function (searchingText) {
    const self = this;

    return await self.all(by.xpath(`option[text()="${searchingText}"]`)).click();
};

/**
 * Select Element By Text(Li)
 */
protractor.ElementFinder.prototype.selectByTextInListElement = async function (searchingList) {
    const self = this;

    return await self.all(by.xpath(`li[text()="${searchingList}"]`)).first();
};

/**
 * Select Element By Text(Td)
 */
protractor.ElementFinder.prototype.selectByTextInTableElement = async function (searchingList) {
    const self = this;

    return await self.all(by.xpath(`td[text()="${searchingList}"]`)).first();
};

/**
 * Mouse Move
 *
 * @example
 * element(by.id('element')).mouseMove();
 *
 * @param world
 * @returns {Promise<void>}
 */
protractor.ElementFinder.prototype.mouseMove = async function (world) {
    const self = this;
    await browser.wait(await until.presenceOf(self), WAITTIMEOUT);
    await browser.wait(await until.visibilityOf(self), WAITTIMEOUT);
    await browser.wait(await until.elementToBeClickable(self), WAITTIMEOUT);

    let x = 10, y = 10;
    await self.getSize().then(function (eleSize) {
        x = eleSize.width / 2;
        y = eleSize.height / 2;
    });

    await browser.sleep(3000);
    if(world !== undefined) {
        await browser.takeScreenshot().then(function(screenShot) {
            world.attach(screenShot, 'image/png');
        });
    }

    await browser.actions().mouseMove(self, {x: x, y: y}).perform();
    await browser.sleep(1500);

    if(world !== undefined) {
        await browser.takeScreenshot().then(function(screenShot) {
            world.attach(screenShot, 'image/png');
        });
    }

    // await CommonUtils.consoleLogs(); //TODO Catch Js error

    return Promise.resolve();
};

/**
 * Drag And Drop an element
 *
 * @example
 * element(by.id('selectPerson').dragNDrop({ x: 100, y:0});
 * element(by.id('selectPerson').dragNDrop(element(by.id('dropPerson'));
 *
 * @param location
 * @returns {Promise<void>}
 */
protractor.ElementFinder.prototype.dragNDrop = async function (location) {
    const self = this;
    await browser.actions().mouseMove(self).perform();
    await browser.actions().mouseDown(self).perform();
    await browser.actions().mouseMove(location).perform();

    return await browser.actions().mouseUp().perform();
};
