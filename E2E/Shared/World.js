"use strict";

const PageObject = require('./World.PageObject');

Given(/^I open(?: "(.*)")? website.?$/, async function (link) {
  link = link || browser.baseUrl;
  await protractor.browser.get(link);
});

Given(/^Related page(?: "(.*)")? is opened.(?: Tarayıcıda bulunan bütün pencereler "(.*)".)?(?: Senaryo ismi "(.*)" olarak başlatılır.)?$/, async function (link, isTabsClosed, zaleniumCookieName) {
    link = link || browser.baseUrl;

    await browser.get(link);
});
