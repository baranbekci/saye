"use strict";

setDefaultTimeout(180000); // asynchronous hooks and steps timeout

let fs = require('fs');
//let readline = require('readline');

Before({tags: "@BLOCKEDBYABUG"}, async () => {
    return await Promise.resolve('pending');
});

BeforeAll(async () => {
    return await Promise.resolve();
});

After(async function (testCase) {

    if (testCase.result.status === Status.FAILED || testCase.result.status === Status.PASSED) {

        global.TestStatus.FAILED += 1;
        global.TestStatus.FAILEDSCN.push({"Feature" : testCase.sourceLocation.uri,"Scenario" : testCase.pickle.name});


        let screenShot = await browser.takeScreenshot();
        // screenShot is a base-64 encoded PNG
        await this.attach(screenShot, 'image/png');

        await browser.switchTo().defaultContent(); // TODO Sometimes map is not showed up and after scenerio is failed.
    }
    else if(testCase.result.status === Status.SKIPPED) {
        global.TestStatus.SKIPPED += 1;
        global.TestStatus.SKIPPEDSCN.push({"Feature" : testCase.sourceLocation.uri,"Scenario" : testCase.pickle.name});
    }
    else if (testCase.result.status === Status.PENDING)
        global.TestStatus.PENDING += 1;
    else
        global.TestStatus.PASSED += 1;

    return await Promise.resolve();
});
