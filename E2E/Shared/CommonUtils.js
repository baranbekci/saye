module.exports = function () {
    let module = {};

    /**
     * Operation System Info
     */
    module.osInfo = () => {
        let os = require('os');
        let userInfo = require('user-info');

        let osType = '';

        switch (os.type()) {
            case "Linux":
                osType = "Linux";
                break;

            case "Darwin":
                osType = "OS X";
                break;

            case "Windows_NT":
                osType = "Windows";
                break;
        }

        global.osType = osType;
        global.osRelease = os.release();
        global.osUserName = userInfo()['username'];

        return {
            OS: osType,
            OSVersion: os.release(),
            UserName: userInfo()['username']
        };
    };

    module.getCliParams = () => {
        let obj = {};
        process.argv.forEach(function(element) {
            if(element.includes("--")) {
                let spl = element.replace('--', '').split('=');
                obj[spl[0]] = spl[1];
            }
        });

        process.params = obj;
    };

    /**
     * get window width and height
     *
     * @returns {{width: number, height: number}}
     */
    module.getWindowWidthNHeight = async function() {

        let browserSize = await browser.driver.manage().window().getSize().then(async function(size) {
            return size;
        });

        return {width: browserSize.width, height: browserSize.height};
    };

    /**
     * get window console logs to typing on cli
     *
     * @returns {Promise<void>}
     */
    module.consoleLogs = async function() {
        await browser.manage().logs().get('browser').then(async function(browserLog) {
            console.log('log: ' + require('util').inspect(browserLog));
        });
    };

    /**
     * Set Browser default behaviours
     */
    module.browserSettings = async () => {
        await browser.manage().window().maximize(); // maximize the browser before executing the feature files

        // browser.ignoreSynchronization = true;
        await browser.waitForAngularEnabled(false);
        // await browser.waitForAngular(false);

        // Todo : Throw TimeoutError: timeout exception
        // await browser.manage().timeouts().setScriptTimeout(SETSCRIPTTIMEOUT);
        // await browser.manage().timeouts().implicitlyWait(IMPLICITLYWAIT);
        // await browser.manage().timeouts().pageLoadTimeout(PAGELOADTIMEOUT);

        await browser.getCapabilities().then(function (capabilities) {
            global.browserName = capabilities.get('browserName');
            global.browserVersion = capabilities.get('version');
        });
    };

    /**
     * Set Cucumber variables to global
     */
    module.cucumberSettings = () => {
        // Set cucumber variable to Global
        const { Given, When, Then, Before, BeforeAll, After, AfterAll, Status, setDefaultTimeout } = require('cucumber');

        global.Given = Given;
        global.When = When;
        global.Then = Then;
        global.Before = Before;
        global.BeforeAll = BeforeAll;
        global.After = After;
        global.AfterAll = AfterAll;
        global.Status = Status;
        global.setDefaultTimeout = setDefaultTimeout;
    };

    /**
     * Chai validation settings
     */
    module.chaiSettings = () => {
        const   chai           = require('chai'),
            chaiAsPromised     = require('chai-as-promised'),
            expect             = chai.expect;

        chai.use(chaiAsPromised);

        global.chai = chai;
        global.expect = expect;
    };

    /**
     * Generate E2E tests report
     * @param fullfill
     * @param reject
     */
    module.generateHtmlReport = (fullfill, reject) => {
        let reportPath = __basefolder + 'Reports/report/';
        const report = require('multiple-cucumber-html-reporter');

        module.clearDir(reportPath); // TODO getting error message "TypeError: JSON.parse(...).map is not a function"

        report.generate({
            jsonDir: __basefolder + 'Reports/',
            reportPath: reportPath,
            saveCollectedJSON: ENVIRONMENT.mergeCucumberJson, // for merge json files to 1 file
            metadata:{
                browser: {
                    name: browserName,
                    version: browserVersion
                },
                device: osUserName,
                platform: {
                    name: osType,
                    version: osRelease
                }
            },
            openReportInBrowser: false,
            displayDuration: true,
            pageTitle: "BB E2E",
            reportName: "E2E Report",
            customData: {
                title: 'Run info',
                data: [
                    {label: 'Machine User Name', value: osUserName},
                    {label: 'Project', value: 'E2E-Project'},
                    {label: 'Release', value: '1.0.0'},
                    {label: 'Execution Start Time', value: testStartDate},
                    {label: 'Execution End Time', value: testFinishDate}
                ]
            }
        });

        fullfill();
    };

    /**
     * GEnerate Zip file from Report
     * @param fullfill
     * @param reject
     */
    module.generateReportZipFile = (fullfill, reject) => {

        if(!ENVIRONMENT.notifications.isGenerateReportZip) {
            fullfill();

            return;
        }

        // require modules
        const fs = require('fs');
        const archiver = require('archiver');

        console.log("Zip entire directory begins");

        // create a file to stream archive data to.
        let output = fs.createWriteStream(__basefolder + '/report.zip');
        let archive = archiver('zip', {
            store: true
        });

        // listen for all archive data to be written
        // 'close' event is fired only when a file descriptor is involved
        output.on('close', function() {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');

            fullfill();
        });

        // This event is fired when the data source is drained no matter what was the data source.
        // It is not part of this library but rather from the NodeJS Stream API.
        // @see: https://nodejs.org/api/stream.html#stream_event_end
        output.on('end', function() {
            console.log('Data has been drained');
        });

        // good practice to catch warnings (ie stat failures and other non-blocking errors)
        archive.on('warning', function(err) {
            if (err.code === 'ENOENT') {
                // log warning
            } else {
                // throw error
                throw err;
            }
        });

        // good practice to catch this error explicitly
        archive.on('error', function(err) {
            throw err;
        });

        // pipe archive data to the file
        archive.pipe(output);

        // append files from a sub-directory, putting its contents at the root of archive
        archive.directory('Reports/', false);

        // finalize the archive (ie we are done appending files but streams have to finish yet)
        // 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
        archive.finalize();
    };

    /**
     * Send Mail
     * @param fullfill
     * @param reject
     */
    module.sendMail = async (fullfill, reject) => {
        let createTables = function (arrayList) {
            let text = "";

            arrayList.forEach(function(obj) {
                let spl = ''+(obj.Feature);

                let str = spl.split("\\");
                if(str.length <= 1)
                    str = spl.split("/");

                text += '<tr><td>'+ (str[4].split("."))[0] +'</td><td>'+ obj.Scenario +'</td></tr>';
            });

            return text;
        };

        if(!ENVIRONMENT.notifications.sendMail) {
            fullfill();

            return;
        }

        const nodeMailer = require('nodemailer');

        let reportName = ENVIRONMENT.reportName;
        let buildUrl = ENVIRONMENT.buildUrl;
        let htmlBody = "";

        console.log("Sending email begins");

        // create reusable transporter object using the default SMTP transport
        let transporter = nodeMailer.createTransport({
            host: 'smtp.gmail.com', //
            port: 465,
            service: 'gmail',
            // secure: true,
            auth: {
                user: 'rudarken7@gmail.com', //
                pass: 'deneme123.',
                domain: 'gmail.com',
            },
          tls: {
            rejectUnauthorized: false
          },
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"E2E Test" <rudarken7@gmail.com>', // sender address
            to: 'baranbekci@gmail.com', // list of receivers
            subject: `E2E Test Raporu ${reportName} 👻 - PASSED: ${global.TestStatus.PASSED} / FAILED: ${global.TestStatus.FAILED}` // Subject line
        };

        if(ENVIRONMENT.notifications.isGenerateReportZip && ENVIRONMENT.notifications.hasAttachmentInMail) {
            htmlBody += `<b>
                   ${reportName} için E2E test raporu arşivlenmiş olarak ektedir.
                   </b> 
                   <br />
                   <br />
                  `;

            mailOptions.attachments = [
                {
                    filename: ENVIRONMENT.reportName + `.zip`,
                    path: 'report.zip' // stream this file
                }
            ];
        }

        htmlBody += `Multiple Cucumber Html : ${buildUrl}E2E-Report/index.html`;

        mailOptions.html = htmlBody;

        if(global.TestStatus.FAILEDSCN.length !== 0 && global.TestStatus.FAILED !== 0)
        {
            let table    = "<br /><br /><table border='1' width='700' cellpadding='2' cellspacing='2'>";
            table       += "<tr><th colspan=\"2\">Hata Veren Senaryolar</th></tr>";
            table       += "<tr><th>Feature</th><th>Scenario</th></tr>";
            table       += createTables(global.TestStatus.FAILEDSCN);
            table       += "</table> ";

            mailOptions.html += table;
        }

        if(global.TestStatus.SKIPPEDSCN.length !== 0 && global.TestStatus.SKIPPED !== 0)
        {
            let table    = "<br /><br /><table border='1' width='700' cellpadding='2' cellspacing='2'>";
            table       += "<tr><th colspan=\"2\">Skipped Edilen Senaryolar</th></tr>";
            table       += "<tr><th>Feature</th><th>Senaryo</th></tr>";
            table       += createTables(global.TestStatus.SKIPPEDSCN);
            table       += "</table> ";

            mailOptions.html += table;
        }
/*
        let tableLines = JSON.parse(await FsPromises.readFile(Properties.paths.featureList));
        if(tableLines !== "") {
            let tableTmp    = "<br /><br /><table border='1' width='700' cellpadding='2' cellspacing='2'>";
            tableTmp       += "<tr><th colspan=\"3\">Çalıştırılan Adım İstatistikleri</th></tr>";
            tableTmp       += "<tr><th>Suite Adı</th><th>Adım Sayısı</th><th>Durum</th></tr>";
            tableTmp       += tableLines.TABLES;
            tableTmp       += "</table> ";

            mailOptions.html += tableTmp;
        }
*/
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log("An error when sending mail" + error);

                reject(error);
            }
            else {
                console.log('Message %s sent: %s', info.messageId, info.response);

                fulfill();
            }
        });
    };

    module.createJsonFileForSummary = async function(path, failedTable, skippedTable) {
        let obj = { };
        const fs = require('fs');
        const fspromises = require('fs').promises;

        if (await fs.existsSync(path)) {
            obj = JSON.parse(await fspromises.readFile(path, 'utf8'));

            obj.PASSED = parseInt(obj.PASSED) + parseInt(global.TestStatus.PASSED);
            obj.FAILED = parseInt(obj.FAILED) + parseInt(global.TestStatus.FAILED);
            obj.SKIPPED = parseInt(obj.SKIPPED) + parseInt(global.TestStatus.SKIPPED);
            obj.BLOCKED = parseInt(obj.BLOCKED) + parseInt(global.TestStatus.PENDING);
            obj.FAILEDTABLE = obj.FAILEDTABLE + failedTable;
            obj.SKIPPEDTABLE = obj.SKIPPEDTABLE + skippedTable;
        }
        else {
            obj = {
                "PASSED": global.TestStatus.PASSED || 0,
                "FAILED": global.TestStatus.FAILED || 0,
                "SKIPPED": global.TestStatus.SKIPPED || 0,
                "BLOCKED": global.TestStatus.PENDING || 0,
                "FAILEDTABLE": failedTable,
                "SKIPPEDTABLE": skippedTable
            };
        }

        await fspromises.writeFile(path, JSON.stringify(obj),{ encoding:'utf8', flag:'w' });

        return await Promise.resolve();
    };

    /**
     * Clear Recursively Dir
     *
     * @param dirPath
     */
    module.clearDir = function (dirPath) {
        dirPath = dirPath || downloadedDir;
        let files = [];
        let fs = require('fs');

        try {
            files = fs.readdirSync(dirPath);
        }
        catch(e) {
            return;
        }

        if (files.length > 0)
            for (let i = 0; i < files.length; i++) {
                if(files[i] === "README.md")
                    continue;

                let filePath = dirPath + '/' + files[i];

                if (fs.statSync(filePath).isFile())
                    fs.unlinkSync(filePath);
                else
                    module.clearDir(filePath);
            }

        // fs.rmdirSync(dirPath);
    };

    /**
     * Delete File from disk
     *
     * @param filePath
     * @returns {Promise<void>}
     */
    module.deleteFile = async function(filePath) {
        let fs = require('fs');

        if (fs.statSync(filePath).isFile())
            return await fs.unlinkSync(filePath);
    };

    /**
     * Get Downloaded File Name
     *
     * @param dirPath
     * @returns {Promise<void>}
     */
    module.getFileName = async function (dirPath) {
        dirPath = dirPath || downloadedDir;

        let fs = require('fs');
        let files;

        try {
            files = await fs.readdirSync(dirPath);
        }
        catch(e) {
            console.log(e);
            return;
        }

        if (files.length > 0)
            for (let i = 0; i < files.length; i++) {
                if(files[i] === "README.md")
                    continue;

                return await files[i];
            }
    };

    /**
     *  Return only base file name without dir
     *
     * @param dirPath
     * @returns {Promise<void>}
     */
    module.getNewestFileName = async function(dirPath) {
        dirPath = dirPath || downloadedDir;

        let fs = require('fs'),
            path = require('path'),
            _ = require('underscore');

        let files = await fs.readdirSync(dirPath);

        // use underscore for max()
        return await _.max(files, function (f) {
            let fullPath = path.join(dirPath, f);

            // ctime = creation time is used
            // replace with mtime for modification time
            return fs.statSync(fullPath).ctime;
        });
    };

    /**
     * Copy File for publishing file over http
     *
     * @param from
     * @param to
     * @returns {Promise<void>}
     */
    module.copyFile = async function(from, to) {
        let fs = require('fs');

        return await fs.createReadStream(from).pipe(fs.createWriteStream(to));
    };

    return module;
}();
