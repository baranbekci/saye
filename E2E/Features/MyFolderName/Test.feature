Feature: Feature Name - Saye

    @FeatureName
    @FeatureNameScenarioName
    Scenario: TC_01 My First Feature
        Given I open "http://78.186.67.187:8080/saye" website.
        When Kullanıcı adı "64444444444" şifre "123" girilir.
        When Birim A. Dinç Birimi seçilir.
        When Birim Seç butonu tıklanır. Birim onaylanır.
        When Yeni Kişi Ekle tıklanır.
        When Kayıt Türü "Misafir" girilir.
        When Kayıt Durumu "Nakil" girilir.
        When Adı "Aykut" girilir.
        When Soyadı "Baytop" girilir.
        When Baba Adı "Dursun" girilir.
        When Anne Adı "Fatma" girilir.
        When Cinsiyet "ERKEK" girilir.
        When Doğum Tarihi "12.04.1979" girilir.
        When Medeni Hali "BEKAR" girilir.
        When Kişi Bilgilerini Kaydet butonu tıklanır.
