"use strict";

const PageObject = require("./Test.PageObject");
let mostExpensiveHotelName = "";

Given(/^Kullanıcı adı "([^"]*)" şifre "([^"]*)" girilir.$/,async function (arg1, arg2) {
  await browser.wait(await until.visibilityOf(PageObject.tcInputField), WAITTIMEOUT);
  await browser.wait(await until.visibilityOf(PageObject.passInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.tcInputField.clear();
  await PageObject.tcInputField.sendKeys(arg1);
  await PageObject.passInputField.clear();
  await PageObject.passInputField.sendKeys(arg2);
  await browser.sleep(500);
  return await PageObject.girisButton.click();
});
When(/^Birim A. Dinç Birimi seçilir\.$/, async function () {
  await browser.wait(await until.visibilityOf(PageObject.birim), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.birim.click();
  await browser.sleep(500);
  return await PageObject.birimSecButton.click();
});
When(/^Birim Seç butonu tıklanır. Birim onaylanır\.$/, async function () {
  await browser.wait(await until.visibilityOf(PageObject.onayEvetButton), WAITTIMEOUT);
  return await PageObject.onayEvetButton.click();
});
When(/^Yeni Kişi Ekle tıklanır\.$/, async function () {
  await browser.wait(await until.visibilityOf(PageObject.yeniKisiEkle), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.yeniKisiEkle.click();
});
When(/^Kayıt Türü "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.kayitTuruInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.kayitTuruInputField.sendKeys(arg1, protractor.Key.ENTER);
});
When(/^Kayıt Durumu "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.kayitTuruInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.kayitDurumuInputField.sendKeys(arg1, protractor.Key.ENTER);
});
When(/^Adı "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.adiInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.adiInputField.sendKeys(arg1);
});
When(/^Soyadı "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.soyadiInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.soyadiInputField.sendKeys(arg1);
});
When(/^Baba Adı "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.babaAdiInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.babaAdiInputField.sendKeys(arg1);
});
When(/^Anne Adı "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.anneAdiInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.anneAdiInputField.sendKeys(arg1);
});
When(/^Cinsiyet "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.cinsiyetInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.cinsiyetInputField.sendKeys(arg1);
});
When(/^Doğum Tarihi "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.dogumTarihiInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.dogumTarihiInputField.sendKeys(arg1);
});
When(/^Medeni Hali "([^"]*)" girilir\.$/, async function (arg1) {
  await browser.wait(await until.visibilityOf(PageObject.medeniHaliInputField), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.medeniHaliInputField.sendKeys(arg1, protractor.Key.ENTER);
});
When(/^Kişi Bilgilerini Kaydet butonu tıklanır\.$/, async function () {
  await browser.wait(await until.visibilityOf(PageObject.kisiBilgileriniKaydetButton), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.kisiBilgileriniKaydetButton.click();
  await browser.sleep(500);
  await browser.wait(await until.visibilityOf(PageObject.onayEvetButton), WAITTIMEOUT);
  await browser.sleep(500);
  await PageObject.onayEvetButton.click();
  await browser.sleep(WAITTIMEOUT);
});
