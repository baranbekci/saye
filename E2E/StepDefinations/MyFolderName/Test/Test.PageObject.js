"use strict";

const test = function () {
  this.tcInputField = element(by.xpath("//input[@id='TC-inputEl']"));
  this.passInputField = element(by.xpath("//input[@id='pass-inputEl']"));
  this.girisButton = element(by.xpath("//span[@id='btnGiris-btnEl']"));

  this.birim = element(by.xpath("//div[contains(text(),'A. Dinç Birimi')]"));
  this.birimSecButton = element(by.xpath("//span[@id='btnBirimSec-btnInnerEl']"));

  this.onayEvetButton = element(by.xpath("//span[@id='button-1006-btnEl']"));

  this.yeniKisiEkle = element(by.xpath("//span[contains(text(),'Yeni Kişi Ekle')]"));

  this.kayitTuruInputField = element(by.xpath("//input[@id='kayitTuru-inputEl']"));
  this.kayitDurumuInputField = element(by.xpath("//input[@id='cbxKayitDurumu-inputEl']"));
  this.adiInputField = element(by.xpath("//input[@name='Adi']"));
  this.soyadiInputField = element(by.xpath("//input[@name='Soyadi']"));
  this.babaAdiInputField = element(by.xpath("//input[@name='BabaAdi']"));
  this.anneAdiInputField = element(by.xpath("//input[@name='AnneAdi']"));
  this.cinsiyetInputField = element(by.xpath("//input[@name='SkrsCinsiyetId']"));
  this.dogumTarihiInputField = element(by.xpath("//input[@name='DogumTarihi']"));
  this.medeniHaliInputField = element(by.xpath("//input[@name='SkrsMedeniHaliId']"));
  this.kisiBilgileriniKaydetButton = element(by.xpath("//span[@id='skbgKimlikBilgileri_btnKaydet-btnEl']"));

};
module.exports = new test();
