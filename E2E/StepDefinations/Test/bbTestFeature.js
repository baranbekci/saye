"use strict";

const PageObject = require("./bbTestFeature.PageObject");
const WorldPageObject = require("../../Shared/World.PageObject");


Given(/^Kullanıcı adı "([^"]*)" girilir.$/,async function (kullaniciAdi) {
    await browser.wait(await until.visibilityOf(PageObject.username), WAITTIMEOUT);
    return await PageObject.username.sendKeys(kullaniciAdi);
});

Given(/^Şifre "([^"]*)" girilir.$/,async function (sifre) {
    await browser.wait(await until.visibilityOf(PageObject.password), WAITTIMEOUT);
    return await PageObject.password.sendKeys(sifre);
});

When(/^Giriş butonu tıklanır.$/,async function () {
    await browser.wait(await until.visibilityOf(PageObject.loginButton), WAITTIMEOUT);
    await browser.wait(await until.elementToBeClickable(PageObject.loginButton), WAITTIMEOUT);
    return await PageObject.loginButton.click();
});

When(/^Veri Kalite "([^"]*)" seçilir.$/,async function (veriKalite) {
    await browser.wait(await until.visibilityOf(PageObject.dataQualitySelection), WAITTIMEOUT);
    return await PageObject.dataQualitySelection.selectByText(veriKalite);
});

When(/^Müteahhit ID "([^"]*)" seçilir.$/,async function (muteahhitId) {
    await browser.wait(await until.visibilityOf(PageObject.constructorNameSelection), WAITTIMEOUT);
    return await PageObject.constructorNameSelection.selectByText(muteahhitId);
});

When(/^Proje Id "([^"]*)" girilir.$/,async function (projeId) {
    await browser.wait(await until.visibilityOf(PageObject.projectIdInputfield), WAITTIMEOUT);
    await PageObject.projectIdInputfield.clear();
    return await PageObject.projectIdInputfield.sendKeys(projeId);
});

When(/^Proje Id çift tıklanır.$/,async function () {
    await browser.wait(await until.visibilityOf(PageObject.projectIdTableData), WAITTIMEOUT);
    await browser.sleep(1000);
    return await PageObject.projectIdTableData.click();
});

When(/^Tamam butonuna tıklanır.$/,async function () {
    await PageObject.okButton.click();
    return await browser.sleep(5000);
});
