"use strict";

const login = function () {

    this.username = element(by.xpath("//*[@id=\"username\"]"));
    this.password = element(by.xpath("//input[@name='password']"));
    this.loginButton = element(by.xpath("//*[@id=\"submitButton\"]"));

    this.dataQualitySelection = element(by.xpath("//*[@id=\"VERI_KALITE\"]"));
    this.constructorNameSelection = element(by.xpath("//*[@id=\"MUTEAHHIT_ID\"]"));
    this.projectIdInputfield = element(by.xpath("//*[@id=\"ProjeNoList_filter\"]/label/input"));
    this.projectIdTableData = element(by.xpath("//tbody/tr[1]//td[2]"));
    this.okButton = element(by.xpath("//button[contains(text(),'Tamam')]"));



};

module.exports = new login();
