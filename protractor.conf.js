/*
Basic configuration to run protractor
@configuration-cli $ node node_modules\protractor\built\cli.js protractor.conf.js --suite=Recovery_RightsHolder --zalenium=true --disableChecks
@configuration-cli $ node node_modules\protractor\built\cli.js protractor.conf.js --suite=Recovery_LocationSelection --cucumberOpts.tags="@AreaOfficer" --cucumberOpts.name=Bölge_Sorumlusu_Güncelleme
**/

let env = require('./environment.js');

exports.config = {

  seleniumAddress: env.seleniumAddress(),

  baseUrl: 'https://www.google.com',

  capabilities: env.capabilities(),

  restartBrowserBetweenTests: false,

  // multiCapabilities: [{
  //     'browserName': 'chrome',
  //     'seleniumAddress' : 'http://localhost:4444/wd/hub'
  // }],

  ignoreUncaughtExceptions: true,

  framework: 'custom',  // set to "custom" instead of cucumber.

  frameworkPath: require.resolve('protractor-cucumber-framework'),  // path relative to the current config file

  suites: {
    "WebsiteTest3": [
      'E2E/Features/MyFolderName/Test.feature'
    ],
  },

  SELENIUM_PROMISE_MANAGER: false,

  // cucumber command line options
  cucumberOpts: {
    require: [
      './E2E/StepDefinations/**/*.js',
      "./E2E/Shared/*.js"
    ],                                                                                  // require step definition files before executing features
    tags: [],                                                                           // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                                                                       // <boolean> fail if there are any undefined or pending steps
    format: [env.cucumberReportFormat(), require.resolve('cucumber-pretty')],       // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    'dry-run': false,                                                                   // <boolean> invoke formatters without executing steps
    compiler: [],                                                                       // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    'format-options': '{"colorsEnabled": true}'
  },

  params: {},

  beforeLaunch: () => {

    let moment = require('moment-business-days');
    moment.locale('en');
    global.testStartDate = moment().format('LLLL');

    let uncaught = require('uncaught');
    uncaught.start();
    uncaught.addListener(function (error) {
      console.log('Uncaught error or rejection: ', error.message);
    });
  },

  onPrepare: async () => {
    let reportName = browser.params.reportName || 'cucumber_report';
    let mergeCucumberJson = process.params.mergeCucumberJson || false;
    let buildUrl = browser.params.buildUrl || "localMachine/";
    let isGenerateReportZip = browser.params.isGenerateReportZip || false;
    let sendMail = browser.params.sendMail || false;
    let hasAttachmentInMail = browser.params.hasAttachmentInMail || false;

    let envParams = {
      reportName: reportName,
      mergeCucumberJson: (mergeCucumberJson === 'true'),
      buildUrl: buildUrl,
      notifications: {
        isGenerateReportZip: (isGenerateReportZip === 'false'),
        sendMail: (sendMail === 'false'),
        hasAttachmentInMail: (hasAttachmentInMail === 'false') // required isGenerateReportZip = true otherwise it does not work!!
      },
    };

    global.ENVIRONMENT = envParams;
    global.EC = global.until = protractor.ExpectedConditions;
    global.__basefolder = __dirname + '/';
    global.downloadedDir = env.downloadedDir();
    global.copyDir = env.copyDir();

    // Default Timeouts
    global.SETSCRIPTTIMEOUT = 180000;
    global.IMPLICITLYWAIT = 3000;
    global.PAGELOADTIMEOUT = 90000;
    global.WAITTIMEOUT = 90000;
    global.WAITTIMEOUTExtreme = 360000;

    // Status
    global.TestStatus = {
      PASSED: 0,
      FAILED: 0,
      SKIPPED: 0,
      PENDING: 0,
      FAILEDSCN: [],
      SKIPPEDSCN: []
    };

    // Date Process
    global.moment = require("moment-business-days");
    moment.locale('tr');

    // Required Settings
    global.Properties = require('./environment');
    // Utils
    global.CommonUtils = require('./E2E/Shared/CommonUtils');
    global.WebUtils = require('./E2E/Shared/WebUtils');

    require('./E2E/Shared/Extenders');
    CommonUtils.osInfo();
    CommonUtils.cucumberSettings();
    await CommonUtils.browserSettings();
    CommonUtils.chaiSettings();
    CommonUtils.clearDir();
  },

  onComplete: () => {
    const moment = require('moment-business-days');
    moment.locale('en');
    global.testFinishDate = moment().format('LLLL');
  },

  onCleanUp: () => {
    return new Promise(function (fulfill, reject) {
      CommonUtils.generateHtmlReport(fulfill, reject);
    })
  }
};
