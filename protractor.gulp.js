/*
Basic configuration to run gulp

For creating bug via JUnit Xml file

@configuration-cli $ node node_modules\gulp\bin\gulp.js --color --gulpfile protractor.gulp.js cucumber:report
**/
const gulp = require('gulp');

// gulp.task('cucumber:report', function() {
//     gulp.src('Reports/cucumber_report.json')
//         .pipe(cucumberXmlReport({strict: true}))
//         .pipe(gulp.dest('Reports'));
// });
//
// function cucumberXmlReport(opts) {
//     let gutil = require('gulp-util'),
//         through = require('through2'),
//         cucumberJunit = require('cucumber-junit');
//
//     return through.obj(function (file, enc, cb) {
//         // If tests are executed against multiple browsers/devices
//         let suffix = file.path.match(/\/cucumber-?(.*)\.json/);
//         if (suffix) {
//             opts.prefix = suffix[1] + ';';
//         }
//
//         let xml = cucumberJunit(file.contents, opts);
//         file.contents = new Buffer(xml);
//         file.path = gutil.replaceExtension(file.path, '.xml');
//         cb(null, file);
//     });
// }
