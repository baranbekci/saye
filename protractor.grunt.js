/*
Basic configuration to run grunt
@configuration-cli $ node node_modules\grunt-cli\bin\grunt --gruntfile protractor.grunt.js multi-e2e --loop=2 --suite=Recovery_LocationSelection
**/

module.exports = function(grunt) {

    grunt.initConfig({
        protractor: {
            options: {
                configFile: "protractor.conf.js", // Default config file
                keepAlive: true, // If false, the grunt process stops when the test fails.
                noColor: false, // If true, protractor will not use colors in its output.
                debug: false,
                args: {
                    // Arguments passed to the command
                }
            },
            e2e: {   // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
                options: {
                    configFile: "protractor.conf.js", // Target-specific config file
                    args: {} // Target-specific arguments
                }
            }
        }
    });

    // Set Default
    grunt.registerTask('default', ['protractor:e2e']);

    // Multi test run
    grunt.registerTask('multi-e2e', function () {
        let N = grunt.option('loop') || 1;
        let tasks = Array();

        for (let i = 0; i < N; i++) {
            tasks.push('protractor:e2e')
        }

        grunt.task.run(tasks);
    });

    //add grunt protractor npm tasks into grunt.
    grunt.loadNpmTasks('grunt-protractor-runner');
};