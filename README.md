# README #

An end to end testing platform for Web project.

### This repository includes ###

End to end specs for Project windows.

### How do I get set up? ###

Pre Condition

    Be sure on e2e folder (cd e2e)

Just run command prompt below

    install.cmd
    
Or Manuel Install

    npm install
    npm run webdriver-update
    
    
### Example Run Scripts ###
    protractor.conf.js --suite=Cografi_Analiz --cucumberOpts.tags="@TesisBazlı" (runs only TesisBazlı in the feature)   
    or
    protractor.conf.js --suite=Cografi_Analiz (runs only Cografi_Analiz feature)
    or
    npm run e2e (runs all suites)

### Dependencies

[Protractor (5.2.2) - E2E Framework][1]

[Web-Driver Manager (12.0.6) - Run Selenium Server][2]

[CucumberJs (4.0.0) - Behaviour-Driven Development Library ][3]

[Chai Assertion Library (4.1.2) - Validation Library][4]

[Protractor Multiple Cucumber Html Reporter (1.3.0) - E2E HTML Report][5]

[1]: http://www.protractortest.org/
[2]: https://github.com/angular/webdriver-manager
[3]: https://github.com/cucumber/cucumber-js
[4]: http://chaijs.com/
[5]: https://github.com/wswebcreation/protractor-multiple-cucumber-html-reporter-plugin
